# xorg-installation

Xorg:
WM: AwesomeWM (X11)
Launcher (X11): Rofi

Pulling the git repo that contains my Xorg setup (no longer in use, use wayland):
```bash
git init 
git remote add origin https://codeberg.org/kmelb/xorg-installation.git
git pull origin main
cd xorg-installation
./xorg_install.bash
# NOTE: run the following line only after entering a xorg enviournment, otherwise it wont work:
./xorg_after_login.bash
```

