# Walrusmoondeer's AwesomeWM config 

#### Dependancies (my AwesomeWM config wont load without these):
- lain-git <-- window border width changing, some statusbar widgets (available in AUR)

#### Integrated but optional (my AwesomeWM is configured to use these): 
- light <-- backlight adjusting 
- xss-lock <-- initializes xsecurelock
- rofi + (.config/rofi + .scripts/) <-- menu's
- xsecurelock <-- screensaver
- xscreensaver <-- themes for xscreenlock 
- networkmanager-applet <-- internet systray icon 
- cbatticon <-- systray battery icon

#### All other applications can be changed in $HOME/.config/awesome/themes/mytheme/theme.lua 

## Show Hotkey list 
- **Alt + H** <-- AwesomeWM hotkey cheatsheet 
#### Launch basic apps
- **Super + Enter|T** <-- Launch terminal (alacritty)
- **Super + B** <-- Launch default web-browser 
- **Super + W** <-- Launch Firefox 
- **Super + F** <-- Launch File Manager (pcmanfm)
#### Basic Window management
- **Super + K** <-- move focus to next window by index
- **Super + J** <-- move focus to previous window by index
- **Super + Shift + K** <-- swap with next window by index
- **Super + Shift + J** <-- swap with previous window by index
- **Super + Q** <-- kill window 
- **Alt + A** <-- Maximize window 
- **Alt + M** <-- Minimize window 
#### Rofi menu's
- **Alt + E** <-- show config menu 
- **Alt + P** <-- show power menu
- **Alt + Tab** <-- show window-switching menu (WIP)
- **Alt + W** <-- do a quick web-search with your default browser
- **Super + Space** <-- launch desktop application
#### Other 
- **Alt + Shift + W** <-- change wallpapers (wallpaper list in .config/awesome/themes/mytheme/theme.lua wallpaper path in .local/share/backgrounds/)
- **Alt + Shift + B** <-- toggle statusbar
