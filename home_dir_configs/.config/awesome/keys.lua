--      ██╗  ██╗███████╗██╗   ██╗███████╗
--      ██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝
--      █████╔╝ █████╗   ╚████╔╝ ███████╗
--      ██╔═██╗ ██╔══╝    ╚██╔╝  ╚════██║
--      ██║  ██╗███████╗   ██║   ███████║
--      ╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝
local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")
local beautiful = require("beautiful")
local iter = beautiful.wallpaper_iter
local res_width = beautiful.res_width 
local res_height = beautiful.res_height 
local wibar_height = beautiful.wibar_height

-- -- The Third-party utilities -- --
local lain = require("lain") -- gives some widgets and utilities.

-- Define mod keys
local modkey = "Mod4"
local altkey = "Mod1"

--# bindsym XF86MyComputer
--# bindsym XF86HomePage
--# bindsym XF86Calculator

-- ontopper variables:
ontop_iter = 0
ontopper_width  = 500
ontopper_height = 500
ontopper_x = {0, 
              res_width - ontopper_width, 
              res_width - ontopper_width, 
              0 }
ontopper_y = {wibar_height, 
              wibar_height, 
              res_height - ontopper_height, 
              res_height - ontopper_height }

-- define module table
local keys = {}

keys.clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- {{{ Key bindings
keys.globalkeys = gears.table.join(
    -- Window Switching
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey,           }, "k", function () awful.client.focus.byidx( 1)    end,
              {description = "focus next by index", group = "client"}),
    awful.key({ modkey,           }, "j", function () awful.client.focus.byidx(-1)    end,
              {description = "focus previous by index", group = "client"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump between urgent clients", group = "client"}),
    awful.key({ modkey,           }, "Tab", function () awful.client.focus.byidx(1)    end,
              {description = "focus next by index", group = "client"}),

    -- Program Spawning
    awful.key({ modkey }, "Return", function () awful.spawn(beautiful.terminal) end,
              {description = "open terminal", group = "launcher"}),
    awful.key({ modkey }, "t", function () awful.spawn(beautiful.terminal) end,
              {description = "open terminal", group = "launcher"}),
    awful.key({ modkey, "Shift" }, "t", function () awful.spawn(beautiful.torrent_client) end,
              {description = "open torrent client", group = "launcher"}),
    awful.key({ modkey }, "v", function () awful.spawn(beautiful.vivaldi) end,
              {description = "open vivaldi", group = "launcher"}),
    awful.key({ modkey }, "b", function () awful.spawn(beautiful.browser) end,
              {description = "open default systmem web-browser", group = "launcher"}),
    awful.key({ modkey }, "w", function () awful.spawn(beautiful.firefox) end,
              {description = "open web-browser (firefox)", group = "launcher"}),
    awful.key({ altkey }, "c", function () awful.spawn("xcalc") end,
              {description = "open calculator", group = "launcher"}),
    awful.key({ modkey }, "c", function () awful.spawn(beautiful.matrix_client) end,
              {description = "open chat client", group = "launcher"}),
    awful.key({ modkey }, "n", function () awful.spawn(beautiful.terminal .. " -e newsboat") end,
              {description = "open newsboat", group = "launcher"}),
    awful.key({ modkey }, "g", function () awful.spawn("steam") end,
              {description = "open steam", group = "launcher"}),
    awful.key({ modkey }, "f", function () awful.spawn(beautiful.filemanager) end,
              {description = "open file manager", group = "launcher"}),
    awful.key({ modkey }, "z", function () awful.spawn(beautiful.notes) end,
              {description = "open note editor", group = "launcher"}),
    awful.key({ modkey }, "e", function () awful.spawn(beautiful.email_client) end,
              {description = "open e-mail client", group = "launcher"}),
    awful.key({ altkey }, "d", function () awful.spawn(beautiful.discord) end,
              {description = "open discord", group = "launcher"}),
    awful.key({ modkey }, "x", function () awful.spawn(beautiful.xmpp_client) end,
              {description = "open xmpp client", group = "launcher"}),
    awful.key({ altkey }, "v", function () awful.spawn(beautiful.password_manager) end,
              {description = "open password_manager", group = "launcher"}),
    --awful.key({ modkey }, "m", function () awful.spawn(music) end,
    --          {description = "open music player", group = "launcher"}),

    -- Rofi Launch Menu's
    awful.key({ altkey }, "p", function ()
        awful.spawn.with_shell("sh $HOME/.scripts/power_menu.sh") end,
              {description = "open 'Power Menu'", group = "launcher"}),
    awful.key({ altkey }, "w", function ()
        awful.spawn.with_shell('sh $HOME/.scripts/websearch_menu.sh') end, 
        {description = "search for something in the browser", group = "launcher"}),
    awful.key({ altkey }, "q", function ()
        awful.spawn.with_shell('sh $HOME/.scripts/quicknote_menu.sh') end, 
        {description = "write down a 'quick note'", group = "launcher"}),
    awful.key({ modkey }, "space", function ()
        awful.spawn.with_shell('sh $HOME/.scripts/search_menu.sh') end, 
        {description = "open 'Search'", group = "launcher"}),
    awful.key({ altkey }, "Tab", function ()
        awful.spawn.with_shell('sh $HOME/.scripts/window-switcher_menu.sh') end,
    -- Windows are organised based on recency, the most recent window is listed first
    -- use ' -window-thumbnail ' if you want to see window thumbnail instead of icons
              {description = "open 'Window-Switcher'", group = "launcher"}),
    awful.key({ altkey }, "e", function ()
            awful.spawn.with_shell("sh $HOME/.scripts/config_menu.sh") end,
              {description = "edit awesome config", group = "awesome"}),
    awful.key({ altkey, "Shift" }, "e", function ()
            awful.spawn.with_shell("sh $HOME/.scripts/emoji_menu.sh") end,
              {description = "List of emojis", group = "awesome"}),
    awful.key({ modkey }, "r", function ()
            awful.spawn.with_shell("sh $HOME/.scripts/run_prompt.sh") end,
              {description = "run prompt", group = "awesome"}),
            
    -- Move to directory (shortcuts)
    awful.key({ modkey }, "d", function () awful.spawn.with_shell(beautiful.filemanager .. " -n Downloads") end,
              {description = "open Downloads folder", group = "launcher"}),
    awful.key({ modkey, "Shift" }, "d", function () awful.spawn.with_shell(beautiful.filemanager .. " -n Documents") end,
              {description = "open Documents folder", group = "launcher"}),
    awful.key({ modkey }, "m", function () awful.spawn.with_shell(beautiful.filemanager .. " -n Music") end,
              {description = "open music directory", group = "launcher"}),
    awful.key({ modkey }, "p", function () awful.spawn.with_shell(beautiful.filemanager .. " -n Practice") end,
              {description = "open music directory", group = "launcher"}),

    -- Volume Keys, use fn key
    awful.key({ }, "XF86AudioLowerVolume", function ()
        --awful.util.spawn("amixer -q -D pipewire sset Master 5%-", false) end,
        awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%", false) end,
              {description = "decrease volume", group = "system"}),
    awful.key({ }, "XF86AudioRaiseVolume", function ()
        --awful.util.spawn("amixer -q -D pipewire sset Master 5%+", false) end,
        awful.util.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%", false) end,
              {description = "increase volume", group = "system"}),
    awful.key({ }, "XF86AudioMute", function ()
        --awful.util.spawn("amixer -D pipewire set Master 1+ toggle", false) end,
        awful.util.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle", false) end,
              {description = "mute volume", group = "system"}),
    -- WIP --
    --awful.key({ }, "XF86AudioPlay", function ()
    --    awful.util.spawn("amixer -D pipewire set Master 1+ toggle", false) end,
    --          {description = "mute volume", group = "system"}),
    --awful.key({ }, "XF86AudioPrev", function ()
    --    awful.util.spawn("amixer -D pipewire set Master 1+ toggle", false) end,
    --          {description = "mute volume", group = "system"}),
    --awful.key({ }, "XF86AudioNext", function ()
    --    awful.util.spawn("amixer -D pipewire set Master 1+ toggle", false) end,
    --          {description = "mute volume", group = "system"}),

    -- Brightness keys ( uses perceived brightness, instead of real brightness )
    -- guide: https://konradstrack.ninja/blog/changing-screen-brightness-in-accordance-with-human-perception/
    awful.key({ }, "XF86MonBrightnessDown",
      function ()
        if light == nil then light = 0.6 end
        light = light - 0.2
        brightness_value = 10 ^ light
        if light < 0 then light = 0; brightness_value = 0.3 end
        awful.spawn("light -S " .. brightness_value)
      end,
      {description = "decrease brightness", group = "system"}),
    awful.key({ }, "XF86MonBrightnessUp",
      function ()
        if light == nil then light = 0.6 end
        light = light + 0.2
        brightness_value = 10 ^ light
        if light > 2 then light = 2 end
        awful.spawn("light -S " .. brightness_value)
      end,
      {description = "increase brightness", group = "system"}),

    -- Redshift Keys, use fn key in combination
    awful.key({ "Shift" }, "XF86MonBrightnessDown",
      function ()
        awful.spawn.with_shell("redshift -x")
      end,
      {description = "disable redshift", group = "system"}),
    awful.key({ "Shift" }, "XF86MonBrightnessUp",
      function ()
        awful.spawn.with_shell("redshift -x; redshift -O 2000")
      end,
      {description = "enable redshift", group = "system"}),

    -- Change Wallpaper/Background
    awful.key({ altkey, "Shift" }, "w",
        function ()
            iter = iter + 1
            if iter > #beautiful.wallpaper_list then iter = 1 end
            wallpaper = ".local/share/backgrounds/" .. beautiful.wallpaper_list[iter]
            gears.wallpaper.maximized(wallpaper, s, true)
            awful.spawn.with_shell('sed -i "s/theme.wallpaper_iter = .*/theme.wallpaper_iter = ' .. iter .. '/" $HOME/.config/awesome/themes/mytheme/theme.lua') 
        end,
        {description = "change wallpaper", group = "awesome"}),

    -- AwesomeWM commands
    awful.key({ altkey }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ altkey, "Shift" }, "b",
        function ()
            myscreen = awful.screen.focused()
            myscreen.mywibox.visible = not myscreen.mywibox.visible
        end,
        {description = "toggle statusbar", group = "awesome"}),
    awful.key({ altkey }, "h", hotkeys_popup.show_help,
              {description = "show help", group="awesome"}),
    awful.key({ modkey }, "l", function() awful.spawn.with_shell(beautiful.lockscreen) end,
              {description = "lock-screen", group="awesome"}),

    -- Resize Window Gaps (lain utility)
    awful.key({ altkey }, "=", function () lain.util.useless_gaps_resize(6) end,
						  {description = "increase gap size", group = "awesome"}),
    awful.key({ altkey }, "-", function () lain.util.useless_gaps_resize(-6) end,
						  {description = "decrease gap size", group = "awesome"}),

    -- Screenshots (sound source: freesound.org)
    awful.key({ modkey, "Shift" }, "s",
        function ()
          awful.spawn.with_shell('shotgun "$HOME/Pictures/$(date "+%d-%m-%Y_%N.png")" -g $(hacksaw) && aplay $HOME/.local/share/sounds/freesound_benboncan_dslr-click.wav')
        end,
        {description = "selection screenshot", group = "awesome"}),
    awful.key({ modkey, }, "Print",
        function ()
          awful.spawn.with_shell('shotgun "$HOME/Pictures/$(date "+%d-%m-%Y_%N.png")" & aplay $HOME/.local/share/sounds/freesound_benboncan_dslr-click.wav')
        end,
        {description = "printscreen screenshot", group = "awesome"}),

	  -- Layout Management
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incmwfact( 0.05) end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incmwfact(-0.05) end,
              {description = "decrease master width factor", group = "layout"}),
    --awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
    --          {description = "increase the number of master clients", group = "layout"}),
    --awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
    --          {description = "decrease the number of master clients", group = "layout"}),
    --awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
    --          {description = "increase the number of columns", group = "layout"}),
    --awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
    --          {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "Tab", function () awful.layout.inc(1) end,
              {description = "select next layout", group = "layout"}),
    awful.key({ altkey }, "o",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal("request::activate", "key.unminimize", {raise = true})
                  end
              end,
              {description = "restore minimized windows (one-by-one)", group = "client"})
              )
keys.clientkeys = gears.table.join(
    -- Window Manipulation 
    awful.key({ modkey }, "q", function (c) c:kill() end,
        {description = "close window", group = "client"}),
    awful.key({ altkey }, "a", function (c)
            c.maximized = not c.maximized
            c:raise()
        end,
        {description = "toggle maximize window", group = "client"}),
    awful.key({ altkey }, "m", function (c) c.minimized = true end,
        {description = "minimize a window", group = "client"}),
    awful.key({ altkey }, "f", 
        function (c)
            c.floating = not c.floating 
            c:raise()
        end,
        {description = "toggle floating for window", group = "client"}),
    awful.key({ altkey }, "t", function (c) c.ontop = not c.ontop end,
        {description = "toggle keep on top", group = "client"}),
    awful.key({ altkey, "Shift" }, "t", function (c) awful.titlebar.toggle(c) end,
              {description = "toggle titlebar", group = "client"}),
    awful.key({ }, "F11",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ altkey, "Shift" }, "o", 
        function (c)
            ontop_iter = ontop_iter + 1
            if ontop_iter > #ontopper_x then 
                ontop_iter = 0
                c.floating = false 
                c.ontop = false
            else 
                c.floating = true
                c.ontop = true 
                c.x = ontopper_x[ontop_iter]
                c.y = ontopper_y[ontop_iter]
                c.height = ontopper_height 
                c.width = ontopper_width 
            end
            c:raise()
        end,
        {description = "cycle thro ontopper modes", group = "client"}),
          
    -- Moving window focus works between desktops
    awful.key({ modkey,           }, "Down", function (c)
            awful.client.focus.global_bydirection("down")
            c:lower()
        end,
        {description = "focus next window up", group = "client"}),
    awful.key({ modkey,           }, "Up", function (c)
            awful.client.focus.global_bydirection("up")
            c:lower()
        end,
        {description = "focus next window down", group = "client"}),
    awful.key({ modkey,           }, "Right", function (c)
            awful.client.focus.global_bydirection("right")
            c:lower()
        end,
        {description = "focus next window right", group = "client"}),
    awful.key({ modkey,           }, "Left", function (c)
            awful.client.focus.global_bydirection("left")
            c:lower()
        end,
        {description = "focus next window left", group = "client"}),

    -- Moving windows between positions works between desktops
    awful.key({ modkey, "Shift"   }, "Left", function (c)
            awful.client.swap.global_bydirection("left")
            c:raise()
        end,
        {description = "swap with left client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Right", function (c)
            awful.client.swap.global_bydirection("right")
            c:raise()
        end,
        {description = "swap with right client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Down", function (c)
            awful.client.swap.global_bydirection("down")
            c:raise()
        end,
        {description = "swap with down client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Up",
        function (c)
            awful.client.swap.global_bydirection("up")
            c:raise()
        end,
        {description = "swap with up client", group = "client"})
)

return keys
