-- _ __ ___   ___ _ __  _   _
-- | '_ ` _ \ / _ \ '_ \| | | |
-- | | | | | |  __/ | | | |_| |
-- |_| |_| |_|\___|_| |_|\__,_|

-- Sourcing Modules
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local beautiful = require("beautiful")

-- Variables
local terminal = beautiful.terminal 
local filemanager = beautiful.filemanager 

local mymenu = {}
-- {{{ Menu
-- Create a launcher widget and a main menu
sysmenu = {
    { "Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
    { "Refresh", awesome.restart },
    { "Log-out", function() awesome.quit() end },
    { "Lock-screen", function() awful.spawn.with_shell(beautiful.lockscreen) end },
    { "Reboot", "systemctl reboot" },
    { "Shutdown", "systemctl poweroff" },
}

applicationmenu = {
    { "Discord", beautiful.discord, beautiful.discord_icon },
    { "Thunderbird", beautiful.email_client, beautiful.thunderbird_icon },
    --{ "Mutt-Wizard", terminal .. " -e mw", beautiful.muttwizzard_icon },
    { "Obsidian", "obsidian", beautiful.obsidian_icon },
    -- { "Pidgin", "pidgin", beautiful.pidgin_icon },
    -- { "Gajim", "gajim", beautiful.gajim_icon },
    -- { "Element", "element-desktop", beautiful.element_icon },
    { "xterm", "xterm", beautiful.xterm_icon },
    -- { "qbittorrent", "qbittorrent", beautiful.qbittorrent_icon },
    -- { "Transmission", "transmission-gtk", beautiful.transmission_icon },
    { "Deluge", beautiful.torrent_client, beautiful.deluge_icon },
    { "Bitwarden", "bitwarden-desktop", beautiful.bitwarden_icon },
    { "Monero-wallet", "monero-wallet-gui", beautiful.monero_icon },
}

browsermenu = {
    { "Vivaldi", beautiful.vivaldi, beautiful.vivaldi_icon },
    { "Firefox", beautiful.firefox, beautiful.firefox_icon },
    -- { "Falkon", beautiful.falkon, beautiful.falkon_icon },
    -- { "Midori", "midori", beautiful.midori_icon },
    -- { "Qutebrowser", "qutebrowser", beautiful.qutebrowser_icon },
    -- { "Amfora", terminal .." -e amfora", beautiful.amfora_icon },
    -- { "Brave", "brave-bin" },
    -- { "Tor Browser", "firejail --apparmor torbrowser-launcher", beautiful.tor_icon },
}

gamemenu = {
    { "Steam", "steam", beautiful.steam_icon },
    { "Minecraft", "multimc", beautiful.minecraft_icon },
    { "CSGO", "steam steam://rungameid/730", beautiful.csgo_icon },
    { "Minetest", "minetest", beautiful.minetest_icon },
    { "Openarena", "openarena", beautiful.quake_icon },
    { "Xonotic", "xonotic-glx", beautiful.xonotic_icon },
    { "SuperTuxKart", "supertuxkart", beautiful.supertuxkart_icon },
    { "Civ6", "steam steam://rungameid/289070", beautiful.civ6_icon },
    { "Northgard", "steam steam://rungameid/466560", beautiful.northgard_icon },
}

develmenu = {
    { "Gimp", "gimp", beautiful.gimp_icon },
    { "Inkscape", "inkscape", beautiful.inkscape_icon },
    { "Kdenlive", "kdenlive", beautiful.kdenlive_icon },
    { "Pinta", "pinta", beautiful.pinta_icon },
    { "VSCodium", "codium --no-sandbox --unity-launch", beautiful.vscode_icon },
    { "Neovim", terminal .." -e nvim", beautiful.neovim_icon},
    { "Calculator", "xcalc", beautiful.xcalc_icon },
    { "Python", terminal .." -e python", beautiful.python_icon },
    { "Lua", terminal .." -e lua", beautiful.lua_icon },
}

mymenu.mymainmenu = awful.menu({ items = { { "Terminal", terminal },
                                    { "Vivaldi", beautiful.vivaldi },
                                    { "Newsboat", terminal .." -e newsboat" },
                                    { "Music", filemanager .." Music" },
                                    { "Applications", applicationmenu },
                                    { "Browsers", browsermenu },
                                    { "Games", gamemenu },
                                    { "Development", develmenu },
                                    { "Htop", terminal .. " -e htop"},
                                    { "System", sysmenu }
                                  }
                        })

mymenu.mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymenu.mymainmenu })

return mymenu
