--     █████╗ ██╗    ██╗███████╗███████╗ ██████╗ ███╗   ███╗███████╗
--    ██╔══██╗██║    ██║██╔════╝██╔════╝██╔═══██╗████╗ ████║██╔════╝
--    ███████║██║ █╗ ██║█████╗  ███████╗██║   ██║██╔████╔██║█████╗
--    ██╔══██║██║███╗██║██╔══╝  ╚════██║██║   ██║██║╚██╔╝██║██╔══╝
--    ██║  ██║╚███╔███╔╝███████╗███████║╚██████╔╝██║ ╚═╝ ██║███████╗
--    ╚═╝  ╚═╝ ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- -- The Third-party utilities -- --
local lain = require("lain") -- gives some widgets and utilities.

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/themes/mytheme/theme.lua")

-- lain (used for changing fore/back ground and changing text appearance)
--local markup = lain.util.markup

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
        -- Actively used modes --
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.floating,
        -- Could consider using these modes --
    -- awful.layout.suit.tile,
    -- awful.layout.suit.tile.bottom,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.max,
        -- Don't like these modes --
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.top,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- Importing Menus
local menu = require("menu")

-- Importing Keybinds
local keys = require("keys")
root.keys(keys.globalkeys)
root.buttons(gears.table.join(
    awful.button({ }, 3, function () menu.mymainmenu:toggle() end)
))

-- Importing Rules
local rules = require("rules")
awful.rules.rules = rules

-- {{{ Wibar (Statusbar)
-- Create a textclock widget
statusbar_clock = wibox.widget.textclock("%a, %d %B %l:%M%p")

-- Spacing
--local space = wibox.widget.separator {
--    orientation = "vertical",
--    forced_width = 3,
--    thickness = 3,
--    color = "#00000000",
--}
local space = wibox.widget{ text = " ", widget = wibox.widget.textbox } -- a space

-- Separator
local vert_sep = wibox.widget.separator {
    orientation = "vertical",
    forced_width = 2,
    thickness = 2,
    color = "#504945",
}

-- pactl based volume widget for pure pipewire setups
local volume = awful.widget.watch(
    "pactl get-sink-volume @DEFAULT_SINK@ | cut -s -d/ -f2,4; pactl get-sink-mute @DEFAULT_SINK@",
    0.1, -- timeout 
    function(widget, stdout)
        local volume = "Volume: "
        for v in stdout:gmatch("(%d+%%)") do volume = volume .. " " .. v end
        if #volume == 8 then volume = "N/A" end
        local mute = string.match(stdout, "Mute: (%S+)") or "N/A"
        -- customize here
        widget:set_markup(volume)
    end
)

--[[
-- infos from mpris clients such as spotify and VLC
-- based on https://github.com/acrisci/playerctl
local mpris, mpris_timer = awful.widget.watch(
    { awful.util.shell, "-c", "playerctl status && playerctl metadata" },
    2,
    function(widget, stdout)
         local escape_f  = require("awful.util").escape
         local mpris_now = {
             state        = "N/A",
             artist       = "N/A",
             title        = "N/A",
             art_url      = "N/A",
             album        = "N/A",
             album_artist = "N/A"
         }

         mpris_now.state = string.match(stdout, "Playing") or
                           string.match(stdout, "Paused")  or "N/A"

         for k, v in string.gmatch(stdout, "'[^:]+:([^']+)':[%s]<%[?'([^']+)'%]?>")
         do
             if     k == "artUrl"      then mpris_now.art_url      = v
             elseif k == "artist"      then mpris_now.artist       = escape_f(v)
             elseif k == "title"       then mpris_now.title        = escape_f(v)
             elseif k == "album"       then mpris_now.album        = escape_f(v)
             elseif k == "albumArtist" then mpris_now.album_artist = escape_f(v)
             end
         end

        -- customize here
        widget:set_text(mpris_now.artist .. " - " .. mpris_now.title)
    end
)
--]]

-- Create a RAM widget (lain widget)
local statusbar_ram = lain.widget.mem {
    settings = function() 
        ram_used = "RAM: " .. mem_now.used .. "Mb"
        ram_prec = "RAM: " .. mem_now.perc .. "%"
        widget:set_markup(ram_used)
        -- colored ==> widget:set_markup(markup.fg.color('#0ED839', ram_used))
    end
}

-- lain CPU usage widget
local statusbar_cpu = lain.widget.cpu {
    settings = function()
        cpu = "CPU: " .. cpu_now.usage .. "%"
        widget:set_markup(cpu)
        -- colored ==> widget:set_markup(markup.fg.color('#3B48E3', cpu))
    end
}

-- lain Computer Temperature
--local statusbar_temp = lain.widget.temp {
--    settings = function()
--        widget:set_markup(coretemp_now)
--    end
--}

-- appid (my free api key): 86af7575001cf8d06ba6b1df5b9e9dac
-- full link: https://api.openweathermap.org/data/2.5/weather?id=456172&appid=86af7575001cf8d06ba6b1df5b9e9dac
local statusbar_weather = lain.widget.weather {
    APPID = "86af7575001cf8d06ba6b1df5b9e9dac",
    --units = "metric",
    --units = "imperial",
    --lang = "en",
    city_id = 456172,  -- id of Riga: 456172
    --weather_na_markup = " weather textbox not available",
    --current_call = "curl -s 'http://api.openweathermap.org/data/2.5/weather?id=%s&APPID=%s'",
    settings = function()
        --weather_temp_actual = math.floor(weather_now["main"]["temp"])
        weather_temp_feels_like = math.floor(weather_now["main"]["feels_like"])
        --weather_pressure = math.floor(weather_now["main"]["pressure"])  -- n hPa
        --weather_humidity = math.floor(weather_now["main"]["humidity"])  -- n%
        --weather_description = weather_now["weather"][1]["description"]:lower()
        -- colored ==>  markup.fg.color('#F996E2', "Weather: " .. weather_temp_feels_like .. "°C (feels like)")
        widget:set_markup("Weather: " .. weather_temp_feels_like .. "°C")
    end
}

-- Create a Disk widget (lain widget)
--local statusbar_disk = lain.widget.fs {
--    settings = function()
--        local disk_percentage = "DISK: " .. fs_now["/"].percentage .. "%"
--        local disk_addon = " (" .. math.floor(fs_now["/"].free) .. " " .. fs_now["/"].units .. " left)"
--        widget:set_markup(disk_percentage .. disk_addon)
--    end
--}

-- lain (powerlines/arrowlines) seperator (WORKS!!)
--local separators = lain.util.separators
--arrl_dl = separators.arrow_left(beautiful.bg_focus, "alpha")
--arrl_ld = separators.arrow_left("alpha", beautiful.bg_focus)

local tasklist_buttons = gears.table.join(
    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                {raise = true}
            )
        end
    end),
    awful.button({ }, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end))

-- Setting Wallpaper
local function set_wallpaper(s)
    local wallpaper_list = beautiful.wallpaper_list
    local wallpaper_iter = beautiful.wallpaper_iter 
    wallpaper = ".local/share/backgrounds/" .. wallpaper_list[wallpaper_iter]
    gears.wallpaper.maximized(wallpaper, s, true)
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table. (!!!DON'T MESS WITH THIS!!!)
    awful.tag({ "1" }, s, awful.layout.layouts[1])

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end)))

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        -- This will show all windows in the current tag:
        --filter  = awful.widget.tasklist.filter.currenttags,
        -- This will show only minimized windows:
        filter  = awful.widget.tasklist.filter.minimizedcurrenttags,
        buttons = tasklist_buttons,
        layout = {
            layout = wibox.layout.flex.horizontal,
            spacing = 8,
            spacing_widget = {
                vert_sep,
                widget = wibox.container.place,
            },
        },
    }

    -- Create the wibox (statusbar)
    s.mywibox = awful.wibar({ position = "top", 
                              screen = s, 
                              height = beautiful.wibar_height,
                              --bg = 
                              --fg = 
                            })

    -- Add widgets to the wibox (statusbar)
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            {
                menu.mylauncher,
                left   = 3,
                right  = 3,
                top    = 3, 
                bottom = 3, 
                widget = wibox.container.margin,
            },
            wibox.layout.margin(awful.widget.launcher({ image = beautiful.icons_firefox, command = beautiful.firefox }),    5, 5, 4, 4), -- 1:Left 2:Right 3:top 4:bottom
            wibox.layout.margin(awful.widget.launcher({ image = beautiful.icons_folder, command = beautiful.filemanager }), 5, 5, 4, 4),
            wibox.layout.margin(awful.widget.launcher({ image = beautiful.icons_terminal, command = beautiful.terminal }),  5, 8, 4, 4),
            vert_sep,
            s.mypromptbox,
            vert_sep,
        },
        {
            {
                s.mytasklist,
                widget = wibox.container.margin,
            }, -- Middle widget
            --bg = "#282828",
            widget = wibox.container.background,
        },
        { -- Right widgets
            --cryptowidget,  -- crypto prices
            --statusbar_disk,
            space, vert_sep, space,
            volume,
            space, vert_sep, space,
            statusbar_ram,
            space, vert_sep, space,
            statusbar_cpu,
            space, vert_sep, space,
            statusbar_weather,
            space, vert_sep,
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            vert_sep, space,
            statusbar_clock,
            space, vert_sep, 
            { 
                { 
                    s.mylayoutbox, 
                    left = 2,
                    right = 2,
                    top = 2, 
                    bottom = 2, 
                    widget = wibox.container.margin,
                }, 
                -- bg = "#000000",
                widget = wibox.container.background,
            }
        },
    }
end)
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    -- rounded corners
        --cr = A cairo content
        --w  = width number The rectangle width
        --h  = height number The rectangle height
        --     radius number the corner radius
    --c.shape = function(cr,w,h)
    --    gears.shape.rounded_rect(cr,w,h,7)
    --end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
						-- When titlebar is dragged, you drag the window
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
						-- When titlebar is clicked, maximized window is (un)maximized
            c.maximized = false
            c:raise()
				    -- When titlebar is dragged, the window is resized
            awful.mouse.client.resize(c)
            if c.floating == false 
                then 
                    awful.layout.set(awful.layout.suit.floating) 
                end
        end),
			  -- When titlebar is clicked, global layout switches to tiled, windows go back to place
        awful.button({ }, 2, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.layout.set(awful.layout.suit.spiral.dwindle)
        end)
    )

    awful.titlebar(c, {
        size = 23, 
        bg_normal = '#2f343f',
        bg_focus  = '#2f343f',
        -- bgimage_normal = "path",
        -- bgimage_focus = "path",
        -- fg_normal = '#ff0000',
        fg_focus = '#aaaaaa',
        -- font = "mononoki-bold"
    }) : setup {
        { -- Left
            awful.titlebar.widget.closebutton    (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.minimizebutton (c),
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.ontopbutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c),
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            --awful.titlebar.widget.iconwidget(c),
            --buttons = buttons,
						awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.minimizebutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.align.horizontal()
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

---- Fullscreen dont have rounded corners
--client.connect_signal("property::fullscreen", function(c)
--    c.shape = function(cr,w,h)
--        gears.shape.rectangle(cr,w,h)
--    end
--end)
--
---- Maximized widows dont have rounded corners
--client.connect_signal("property::maximized", function(c)
--    c.shape = function(cr,w,h)
--        gears.shape.rectangle(cr,w,h)
--    end
--end)
---- Only Floating windows have titlebars
--client.connect_signal("property::floating", function(c)
--    if c.floating then
--        awful.titlebar.show(c)
--    else
--        awful.titlebar.hide(c)
--    end
--end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
