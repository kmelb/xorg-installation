--      ██████╗ ██╗   ██╗██╗     ███████╗███████╗
--      ██╔══██╗██║   ██║██║     ██╔════╝██╔════╝
--      ██████╔╝██║   ██║██║     █████╗  ███████╗
--      ██╔══██╗██║   ██║██║     ██╔══╝  ╚════██║
--      ██║  ██║╚██████╔╝███████╗███████╗███████║
--      ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝╚══════╝

local awful = require("awful")
local beautiful = require("beautiful")
local keys = require("keys")

-- Rules
-- Rules to apply to new clients (through the "manage" signal).

-- class = "Steam": focus = false, urgent = false, titlebars_enabled = true, floating = true, below = true, minimized = true, lower = true, focusable = false,
-- class = "csgo_linux64": above = true, fullscreen = true, maximized = true, ontop = true, urgent = true, focus = true
-- another fix would be to launch csgo only in tag 2 and keep all steam files on tag 1

local rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = keys.clientkeys,
                     buttons = keys.clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = true }
    },
    { rule = { class = "mpv" },
      properties = { ontop = true, floating = true, width = 854, height = 480, x = 533 + 16, y = 300 } },
    { rule = { class = "imv" },
      properties = { ontop = true, floating = true} },
    { rule = { name = "Calc" },
      properties = { floating = true } },
    { rule = { class = "Pidgin" }, --vc16
      properties = { floating = true } },
    { rule = { name = "NoiseTorch" },
      properties = { floating = true, x = 360, y = 130 } },
    --{ rule = { class = "Vivaldi-stable" },
    --  properties = { name = "Vivaldi" } },  -- Every time i change tab it goes back to usual
    { rule = { class = "csgo_linux64" },
      properties = { ontop = true, 
                     maximized = true,
                     --fullscreen = true, 
                   } 
    },
    { rule = { class = "Steam" },
      properties = { titlebars_enabled = false, floating = true } }
}

return rules
