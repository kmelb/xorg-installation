--  _____ _   _ _____ __  __ _____
-- |_   _| | | | ____|  \/  | ____|
--   | | | |_| |  _| | |\/| |  _|
--   | | |  _  | |___| |  | | |___
--   |_| |_| |_|_____|_|  |_|_____|
  
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local themes_path = "~/.config/awesome/themes/"

local theme = {}

-- NOTE: if wallpaper size is native to screen, 
-- /* then it will load faster, as witnessed when changing it
theme.wallpaper_list = {
    "bright_mountain_lake.jpg",
    "misty_epic_mountain.jpg",
    "fall_of_roman_empire.jpg",
    "dark_forest.jpg",
    "death-of-julius-caesar.jpg",
    "mist-terrain.jpg",
    "snow_sunset_lake.jpg",
    "swiss-alps.jpg"
    }
theme.wallpaper_iter = 2

-- Desktop Size Settings:
theme.res_width = 1920 -- resolution width (x axis)
theme.res_height = 1080 -- resolution height (y axis)
theme.wibar_height = 22 -- statusbar's vertical thickness

-- Some Global Variables:
theme.terminal = "alacritty"
--theme.notes = "firejail --apparmor obsidian"
theme.notes = "obsidian"
theme.falkon = "firejail --apparmor falkon"
theme.firefox = "firejail --apparmor firefox"
theme.browser = "firejail --apparmor $BROWSER"
theme.filemanager = "pcmanfm"
theme.xmpp_client = "dino"
theme.editor = "nvim"
theme.password_manager = "bitwarden-desktop"
theme.vivaldi = "vivaldi-stable" -- fido2 key doesnt work on a firejailed browser for some reason
-- and whilist in firejail, cant merge windows, cant close one window without the onther crashing.
--theme.vivaldi = "firejail --apparmor vivaldi-stable"
theme.lockscreen = "env XSECURELOCK_SAVER=saver_xscreensaver XSECURELOCK_PASSWORD_PROMPT=asterisks xsecurelock"
theme.torrent_client = "deluge-gtk" --problem with deluge is that it doesnt like firejail, where as transmission likes firejail
theme.email_client = "firejail --apparmor thunderbird"
theme.document_viewer = "firejail --apparmor zathura"
--theme.matrix_client = "firejail --apparmor element-desktop"
theme.matrix_client = "element-desktop"
--theme.discord = "firejail --apparmor discord"
theme.discord = "discord"
-- # NOTE: some apps wont launch with firejail, no default profile available?
-- /usr/lib/electron17/chrome-sandbox has to be owned by root and has mode 4755 for electron to work (fuck it)

-- Font Settings:
--theme.font          = "jetbrains-mono bold 11"
theme.font          = "mononoki NerdFont bold 13"

-- Border Theme:
theme.useless_gap   = dpi(0) -- dpi(0) <-- default, used 6 before
theme.border_width  = dpi(2)
theme.border_normal = "#222222" -- same as titlebar color
-- theme.border_focus  = "#535d6c"
theme.border_focus  = "#e1acff" -- from qtile
theme.border_marked = "#91231c"

-- Hotkeys Popup Theme (keybinding help popup)
theme.hotkeys_bg = "#222222"
theme.hotkeys_fg = "#d0d0d0" -- description and non-mod key color
theme.hotkeys_border_width = theme.border_width
theme.hotkeys_border_color = theme.border_focus
--  theme.hotkeys_shape
theme.hotkeys_modifiers_fg = "#a0a0a0" -- modkey, altkey color
--theme.hotkeys_label_bg = "#333333"
--theme.hotkeys_laber_fg = "#333333"
theme.hotkeys_font = theme.font
theme.hotkeys_description_font = "jebrains-mono bold 9"
theme.hotkeys_group_margin = 20 -- spaces between grouped hotkeys, and the border and other hotkeys

-- Some Useful Emojis (font/text)
--theme.firefox_icon = ""
--theme.terminal_icon = "" --  ,  ,  
--theme.filemanager_icon = "ﱮ" --  , 
--theme.vim_icon = ""

-- Tasklist Theme: --
-- tasklist_[bg|fg]_[focus|urgent]
theme.tasklist_bg_minimize = "#222222"
theme.tasklist_fg_minimize = "#aaaaaa"
theme.tasklist_align = "center" -- default == left
--theme.tasklist_font = "hermit bold 11"
--theme.tasklist_plain_task_name = true
--theme.tasklist_disable_icon = true
theme.tasklist_spacing = 3

-- Icons (SVG/Image)
theme.icons_firefox = themes_path .. "/mytheme/icons/firefox.svg"
theme.icons_folder  = themes_path .. "/mytheme/icons/folder.svg"
theme.icons_terminal = themes_path .. "/mytheme/icons/terminal.svg"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c" -- default
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff" -- default
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

--theme.master_width_factor = 0.6 -- 0.5  <-- default (dont work for some reason?!)
--theme.master_count = 2 -- 1 <-- default
--theme.column_count = 2 -- 1 <-- default
--theme.maximized_honor_padding = false -- true <-- default
theme.maximized_hide_border = true -- false <-- default
theme.gap_single_client = false -- true <-- default
theme.fullscreen_hide_border = true
-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."mytheme/submenu.png"
theme.menu_height = dpi(20)
theme.menu_width  = dpi(160)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."mytheme/titlebar/close_normal.png"
-- theme.titlebar_close_button_focus  = themes_path.."mytheme/titlebar/close_focus.png" -- default

theme.titlebar_minimize_button_normal = themes_path.."mytheme/titlebar/minimize_normal.png"
-- theme.titlebar_minimize_button_focus  = themes_path.."mytheme/titlebar/minimize_focus.png" -- default

theme.titlebar_ontop_button_normal_inactive = themes_path.."mytheme/titlebar/ontop_normal_inactive.png"
--theme.titlebar_ontop_button_focus_inactive  = themes_path.."mytheme/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."mytheme/titlebar/ontop_normal_active.png"
--theme.titlebar_ontop_button_focus_active  = themes_path.."mytheme/titlebar/ontop_focus_active.png"

--theme.titlebar_sticky_button_normal_inactive = themes_path.."mytheme/titlebar/sticky_normal_inactive.png"
--theme.titlebar_sticky_button_focus_inactive  = themes_path.."mytheme/titlebar/sticky_focus_inactive.png"
--theme.titlebar_sticky_button_normal_active = themes_path.."mytheme/titlebar/sticky_normal_active.png"
--theme.titlebar_sticky_button_focus_active  = themes_path.."mytheme/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."mytheme/titlebar/floating_normal_inactive.png"
--theme.titlebar_floating_button_focus_inactive  = themes_path.."mytheme/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."mytheme/titlebar/floating_normal_active.png"
--theme.titlebar_floating_button_focus_active  = themes_path.."mytheme/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."mytheme/titlebar/maximized_normal_inactive.png"
--theme.titlebar_maximized_button_focus_inactive  = themes_path.."mytheme/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."mytheme/titlebar/maximized_normal_active.png"
--theme.titlebar_maximized_button_focus_active  = themes_path.."mytheme/titlebar/maximized_focus_active.png"

-- You can use your own layout icons like this:
--theme.layout_fairh = themes_path.."mytheme/layouts/fairhw.png"
--theme.layout_fairv = themes_path.."mytheme/layouts/fairvw.png"
theme.layout_floating  = themes_path.."mytheme/layouts/floatingw.png"
--theme.layout_magnifier = themes_path.."mytheme/layouts/magnifierw.png"
theme.layout_max = themes_path.."mytheme/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."mytheme/layouts/fullscreenw.png"
--theme.layout_tilebottom = themes_path.."mytheme/layouts/tilebottomw.png"
--theme.layout_tileleft   = themes_path.."mytheme/layouts/tileleftw.png"
--theme.layout_tile = themes_path.."mytheme/layouts/tilew.png"
--theme.layout_tiletop = themes_path.."mytheme/layouts/tiletopw.png"
--theme.layout_spiral  = themes_path.."mytheme/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."mytheme/layouts/dwindlew.png"
--theme.layout_cornernw = themes_path.."mytheme/layouts/cornernww.png"
--theme.layout_cornerne = themes_path.."mytheme/layouts/cornernew.png"
--theme.layout_cornersw = themes_path.."mytheme/layouts/cornersww.png"
--theme.layout_cornerse = themes_path.."mytheme/layouts/cornersew.png"

-- Same color titlebar buttons
theme.titlebar_close_button_focus = theme.titlebar_close_button_normal
theme.titlebar_minimize_button_focus = theme.titlebar_minimize_button_normal
theme.titlebar_floating_button_focus_inactive = theme.titlebar_floating_button_normal_inactive
theme.titlebar_floating_button_focus_active = theme.titlebar_floating_button_normal_active
theme.titlebar_maximized_button_focus_inactive = theme.titlebar_maximized_button_normal_inactive
theme.titlebar_maximized_button_focus_active = theme.titlebar_maximized_button_normal_active
theme.titlebar_ontop_button_focus_active = theme.titlebar_ontop_button_normal_active
theme.titlebar_ontop_button_focus_inactive = theme.titlebar_ontop_button_normal_inactive
-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
