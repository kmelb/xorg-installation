#!/usr/bin/env sh 
menu_text=" AwesomeWM Config Files\n rc.lua\n theme.lua\n keys.lua\n menu.lua\n rules.lua\n /mytheme\n Rofi Config Files\n config_menu.bash\n power_menu.bash\n search_menu.bash\n window-switcher_menu.bash\n websearch_menu.bash\n quicknote_menu.bash"
lines=14
terminal="alacritty"
if [ $XDG_SESSION_TYPE = "x11" ]; then
  chosen=$(printf "${menu_text}" | rofi -dmenu -theme ericmurphyxyz -theme-str "listview {lines: ${lines};}" -matching prefix)
  #terminal="alacritty"
else
  #chosen=$(printf "${menu_text}" | fuzzel -b 222222ff -s 333333ff -d -l "${lines}" ) 
  chosen=$(printf "${menu_text}" | wofi "${lines}" ) 
  #terminal="foot"
fi
# nf-fa-code =  
# nf-mdi-folder= 
case "$chosen" in 
  " rc.lua") $terminal -e nvim $HOME/.config/awesome/rc.lua ;;
  " theme.lua") $terminal -e nvim $HOME/.config/awesome/themes/mytheme/theme.lua ;;
  " keys.lua") $terminal -e nvim $HOME/.config/awesome/keys.lua ;;
  " menu.lua") $terminal -e nvim $HOME/.config/awesome/menu.lua ;;
  " rules.lua") $terminal -e nvim $HOME/.config/awesome/rules.lua ;;
  " /mytheme") $terminal -e pcmanfm $HOME/.config/awesome/themes/mytheme ;;
  " Default Config") $terminal -e nvim -R $HOME/.config/awesome/backup/default.lua ;;
  " config_menu.bash") $terminal -e nvim $HOME/.scripts/config_menu.bash ;;
  " power_menu.bash") $terminal -e nvim $HOME/.scripts/power_menu.bash ;;
  " search_menu.bash") $terminal -e nvim $HOME/.scripts/search_menu.bash ;;
  " window-switcher_menu.bash") $terminal -e nvim $HOME/.scripts/window-switcher_menu.bash ;;
  " websearch_menu.bash") $terminal -e nvim $HOME/.scripts/websearch_menu.bash ;;
  " quicknote_menu.bash") $terminal -e nvim $HOME/.scripts/quicknote_menu.bash ;;
  *) exit 1 ;;
esac
