#!/usr/bin/env sh 
# NOTE: window-switcher does not work on wayland, it can only run if the window manager supports it.
# -window-thumbnail
# \* sway uses swaymsg for window-switcher to work
if [ $XDG_SESSION_TYPE = "x11" ]; then
  rofi -modi window -show window -theme "$HOME/.config/rofi/themes/window_switcher" -show-icons #-icon-theme "Papirus"
  #rofi -modi window -show window -window-thumbnail -theme "$HOME/.config/rofi/themes/window_switcher" -icon-theme "Papirus"
#else
  #wofi -modi window -show window
fi
