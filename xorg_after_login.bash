#!/usr/bin/env bash

# Variables
keymap="lv"

# Colors for colorful prompts/text:
RED='\033[0;31m'
NC='\033[0m' # No Color
BLUE='\033[0;34m'

# XXX: these setting can only be applied when booted in a Xorg enviornment (desktop)
xset s 300 5 # setting screensaver turnon period to 5mins

echo -e "${BLUE}Setting currect keymap${NC}"
sudo localectl --no-convert set-x11-keymap ${keymap} || { echo -e "${RED}failed to change local language to ${keymap}${NC}"; exit; }
