#!/usr/bin/env bash

# Packages for my Xorg windows manager config to work:
desktop_pkgs=(xorg-server sddm awesome xterm shotgun hacksaw rofi xss-lock xsecurelock xsel)

read -rp "Install a basic graphical enviornment? [Y/n]: " graphical_env

case "${graphical_env}" in 
  [Nn]) 
    echo "${RED}Graphical enviornment won't be installed.${NC}" ;;
  *) 
    echo -e "${BLUE}Contents of 'desktop_pkgs' will be installed as graphical enviornment${NC}" 
    sudo pacman --noconfirm --needed -S "${desktop_pkgs[@]}"
    yay -S lain-git # For AwesomeWM
esac

# Installing my Desktop enviournment config files:
cp -rnv home_dir_configs/.scripts/* "$HOME"/.scripts/
cp -rnv home_dir_configs/.config/* "$HOME"/.config/
cp -nv home_dir_configs/.xprofile "$HOME"/
# NOTE: the -n argument disables overwriting
